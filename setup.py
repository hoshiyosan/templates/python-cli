from setuptools import setup, find_packages
import cliname as src

setup(
    name = src.__name__,
    version = src.__version__,
    packages = find_packages(),
    long_description = src.__doc__,
    entry_points = {
        'console_scripts': [
            '%s = %s.__main__:main'%(src.__package__, src.__name__)
        ]
    })
