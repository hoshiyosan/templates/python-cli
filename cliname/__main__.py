from .components import parser, subparsers
from . import actions

def main():
    args = parser.parse_args()
    if getattr(args, 'func', False):
        args.func(args)

# parse arguments
if __name__ == "__main__":
    main()
