import argparse
from . import __version__, __author__, __doc__, __name__

# init parser
parser = argparse.ArgumentParser(__name__, description=__doc__)
parser.add_argument('--version', '-V', action='version', version=__version__)

subparsers = parser.add_subparsers(dest="command") 
