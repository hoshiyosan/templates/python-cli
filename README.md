# CLI program template
> A simple template to quick start creating a CLI program in Python.

## Step to begin with
* rename package `<cli_name>`
* in `setup.py`, replace import `<cli_name>`
* in `__init__.py`, replace variables with yours
